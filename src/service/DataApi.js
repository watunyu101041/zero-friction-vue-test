import axios from "axios"

export default {
    apiGetData: function() {
        axios.get('https://dummyjson.com/products?limit=3')
            .then((data) => {
                console.log('data', data);
                return data
            })
            .catch((err) => {
                console.log(err)
            })
    }
}
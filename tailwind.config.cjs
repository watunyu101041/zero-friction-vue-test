/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    extend: {
      colors: {
        'yellow-d9ef0c' : '#d9ef0c',
        'gray-c4c4c4' : '#c4c4c4',
      },
      fontFamily: {
        main: ["main"],
      },
      fontSize: {
        26: ["1.625rem", "1.2"],
        28: ["1.75rem", "1.2"],
      }
    },
  },
  plugins: [],
}
